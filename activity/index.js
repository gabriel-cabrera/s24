const getCube = Math.pow(2,3);
console.log(`The cube of 2 is ${getCube}`);

const completeAddress = ['258', 'Washington Ave NW', 'California', '90011'];

const [houseNumber, street, state, zipcode] = completeAddress;

console.log(`I live at ${houseNumber} ${street}, ${state} ${zipcode}`);

const animal = {
	animalName: "Lolong",
	animalType: "saltwater crocodile",
	animalWeight: "1075",
	animalSize: "20 ft 3 in"
};

const {animalName, animalType, animalWeight, animalSize} = animal;

console.log(`${animalName} was a ${animalType}. He weighed at ${animalWeight} with a measurement of ${animalSize}`);

const numbers = [1, 2, 3, 4, 5];

numbers.forEach((number) =>{
console.log(`${number}`);
})

let reduceNumber = numbers.reduce((total, number) =>
	total += number);

console.log(reduceNumber);


class Dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const myDog = new Dog("Frankie", 5, "Miniature Dachshund");

console.log(myDog);
